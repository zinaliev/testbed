package zinaliev.testbed;

/**
 * @author rzinalie
 */
public class EntryPoint {

   public static void main(String[] args) {
      TestbedService service = new TestbedService();

      System.out.println("Service name is: " + service.getName());
   }
}
