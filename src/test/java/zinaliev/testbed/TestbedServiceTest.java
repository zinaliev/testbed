package zinaliev.testbed;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author rzinalie
 */
public class TestbedServiceTest {

   @Test
   public void testGetName_ReturnsFinalServiceName() throws Exception {
      TestbedService service = new TestbedService();

      assertEquals(TestbedService.NAME, service.getName());
   }
}