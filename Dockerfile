FROM thetallgrassnet/alpine-java

MAINTAINER Roman Zinaliev <zinaliev@gmail.com>

USER root

ADD build/libs/testbed.jar /opt/testbed/testbed.jar

WORKDIR /opt/testbed

# start container
CMD ["java", "-jar", "testbed.jar"]